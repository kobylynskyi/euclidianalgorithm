import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EuclideanView extends JFrame {

    private JPanel mainPanel;
    private JTextField aTextField;
    private JTextField bTextField;
    private JButton goButton;
    private JTextField gcdTextField;
    private JPanel logPanel;
    private JTextArea textArea;

    public EuclideanView() {
        initComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        goButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String aString = aTextField.getText();
                String bString = bTextField.getText();
                if (aString != null && aString.length() > 0 && aString.matches("[0-9]+") &&
                        bString != null && bString.length() > 0 && bString.matches("[0-9]+")) {
                    int a = Integer.parseInt(aString);
                    int b = Integer.parseInt(bString);
                    if (a > 0 && b > 0) {
                        textArea.setText(String.format("Finding the GCD of a = %s and b = %s:\n", a, b));
                        while (b != 0) {
                            int modulo = a % b;
                            textArea.append(String.format("%d = %d x %d + %d\n", a, a/b, b, modulo));
                            a = b;
                            b = modulo;
                        }

                        String gcd = String.valueOf(Euclide.getGCD(a, b));

                        textArea.append(String.format("Greatest common divisor = %s", gcd));
                        gcdTextField.setText(gcd);
                    }
                }
            }
        });
    }

    private void initComponents() {
        setResizable(false);
        setContentPane(mainPanel);
        setLocation(150, 150);


        logPanel.setLayout(new BoxLayout(logPanel, BoxLayout.PAGE_AXIS));
        textArea = new JTextArea();
        JScrollPane scroll = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        logPanel.add(scroll);
        textArea.setRows(7);
        textArea.setLineWrap(true);
    }

    public static void main(String[] args) {
        new EuclideanView().setVisible(true);
    }
}
